# Step 0: Prepare your workspace
Start your container:
```shell
sudo docker run -p 5432:5432 --name demo1 --detach -it postgresai/seamless:v3
```

And then start a bash session inside it:
```shell
sudo docker exec -it demo1 bash
```

[back to README «](./README.md) | [» to step 001](./001.md)
