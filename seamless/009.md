## Step 009: EXPLAIN plan visualization
Visualize `EXPLAIN` data right from psql. For this, let's use a small utility called [plan-exporter](https://github.com/agneum/plan-exporter) (already installed).

Make `plan-exporter` listen the output in your psql:
```
\o | plan-exporter
```

Now run any `EXPLAIN`. Some arbitrary example:
```sql
explain (analyze, buffers)
select *
from pgbench_accounts
join pgbench_branches using (bid)
where aid < 1000
order by bbalance desc
limit 20;
```

You'll see both the traditional output of `EXPLAIN` and additional message suggesting to submit the data to [explain.depesz.com](https://explain.depesz.com). To confirm, enter the following:
```
\qecho Y
```

You should get the link to `EXPLAIN` data visualization at [explain.depesz.com](https://explain.depesz.com). Check it out.

Now, let's do another visualization – this time, using [explain.dalibo.com](https://explain.dalibo.com) (PEV2). Switch to this service:

```
\o | plan-exporter --target=dalibo
```

Again, run some `EXPLAIN`:
```
explain (analyze, buffers)
select *
from pgbench_accounts
join pgbench_branches using (bid)
where aid < 1000
order by bbalance desc
limit 20;
```

And as before, confirm posting the data:
```
\qecho Y
```

Check out the new visualization. Pay attention to minimap on the left upper corner, and `BUFFERS` data visualization.

---

[back to 008 «](./008.md) | [» to step 010](./010.md)