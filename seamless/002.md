# Step 002: Check currently running queries
- Using `psql`:
    ```shell
    psql demo1 -c 'select * from pg_stat_activity'
    ```
- Using `pgcenter` (alteady installed):
    ```shell
    pgcenter top demo1
    ```

To exit any time, press `q` (works both for psql's wide output and for pgCenter).

---

[back to 001 «](./001.md) | [» to step 003](./003.md)