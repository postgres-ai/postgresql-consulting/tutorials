## Step 008: A case with lower row count
Let's get rid of the existing data in `pgbench_history` and then re-analyze.

```sql
truncate pgbench_history;
```

Reset `pg_stat_statements` stats:
```sql
select pg_stat_statements_reset();
```

And after a moment of two, check the "Top-5 by `total_exec_time`"` list again:
```sql
select * from pg_stat_statements order by total_exec_time desc limit 5;
```

How would you explain the metrics you observe now, compared to the results in the previous steps?

---

[back to 007 «](./007.md) | [» to step 009](./009.md)
